import { FieldHeader } from "../common/style";
import { IItem } from "../common/types";
import { DataTable } from "grommet";
import Spinner from "./Spinner";
import { useResourceByName } from "../hooks";

const ResourceItem: React.FC<IItem> = ({ name }) => {
  const { data: resources, isLoading } = useResourceByName(name);

  const columns = [
    {
      property: "ConsumedQuantity",
      header: <FieldHeader> Consumed Quantity</FieldHeader>,
      fieldType: "header",
    },

    {
      property: "Cost",
      header: <FieldHeader> Cost</FieldHeader>,
      fieldType: "header",
    },
    {
      property: "Date",
      header: <FieldHeader> Date</FieldHeader>,
      fieldType: "header",
    },
    {
      property: "ResourceLocation",
      header: <FieldHeader> Resource Location</FieldHeader>,
      fieldType: "header",
    },
    {
      property: "UnitOfMeasure",
      header: <FieldHeader> Unit Of Measure</FieldHeader>,
      fieldType: "header",
    },

    {
      property: "Location",
      header: <FieldHeader> Location</FieldHeader>,
      fieldType: "header",
    },
  ];

  if (isLoading) {
    return <Spinner />;
  }

  return (
    <>
      {resources && (
        <DataTable
          columns={columns}
          data={resources}
          paginate={false}
        ></DataTable>
      )}
    </>
  );
};

export default ResourceItem;
