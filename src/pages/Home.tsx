import { Box, Tab, Tabs } from "grommet";
import ApplicationList from "./ApplicatinList";
import RowList from "./RowList";
import ResourceList from "./ResourceList";
import { useApplications, useResources } from "../hooks";
import Spinner from "../components/Spinner";

const Home = () => {
  const { data: applications, isLoading: appIsLoading } = useApplications();
  const { data: resources, isLoading: resIsLoading } = useResources();

  if (appIsLoading || resIsLoading) {
    return <Spinner />;
  }

  return (
    <Tabs>
      <Tab title="Row List">
        <Box pad="medium">
          <RowList appList={applications || []} resList={resources || []} />
        </Box>
      </Tab>
      <Tab title="Applications">
        <Box pad="medium">
          <ApplicationList appList={applications || []} />
        </Box>
      </Tab>
      <Tab title="Resources">
        <Box pad="medium">
          <ResourceList resList={resources || []} />
        </Box>
      </Tab>
    </Tabs>
  );
};

export default Home;
