import { useQuery } from "react-query";
import { axiosGet } from "../lib/axiosClient";

const useResources = () => {
  return useQuery<string[]>(
    ["resources"],
    async () => {
      const { data } = await axiosGet("api/resources");
      return data;
    },
    {
      refetchOnWindowFocus: false,
    }
  );
};

export default useResources;
