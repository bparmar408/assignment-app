import { Box, DataTable, Select, Text } from "grommet";
import { useEffect, useState } from "react";
import styled from "styled-components";
import { FieldHeader } from "../common/style";
import { IParam, IRow } from "../common/types";
import Spinner from "../components/Spinner";
import { useRows } from "../hooks";

const ToolBar = styled.ul`
  display: flex;
  flex-direction: row;
  margin-top: 34px;
  margin-bottom: 28px;
  justify-content: space-between;
  flex-wrap: wrap;
  padding: 0px;
  li {
    list-style-type: none;
  }
`;

const TextField = styled(Text)`
  font-size: 20px;
  text-align: right;
  font-weight: 500;
  color: #000000;
  margin-right: 8px;
`;

const FlexBox = styled(Box)`
  display: flex;
  flex-direction: row;
  align-items: center;
`;

const RowList: React.FC<IParam> = ({ appList, resList }) => {
  const { data: rows, isLoading } = useRows();

  const [list, setList] = useState<IRow[]>([]);
  const [application, setApplication] = useState<string>("");
  const [resource, setResource] = useState<string>("");

  useEffect(() => {
    if (rows) {
      setList(rows);
    }
  }, [rows]);

  if (isLoading) {
    return <Spinner />;
  }

  const filterList = (appName: string, resName: string) => {
    if (rows) {
      const newList = rows?.filter((x) => {
        return (
          (appName ? appName === x.ResourceGroup : true) &&
          (resName ? resName === x.ServiceName : true)
        );
      });
      setList(newList);
    }
  };

  const columns = [
    {
      property: "ConsumedQuantity",
      header: <FieldHeader> Consumed Quantity</FieldHeader>,
      fieldType: "header",
    },

    {
      property: "Cost",
      header: <FieldHeader> Cost</FieldHeader>,
      fieldType: "header",
    },
    {
      property: "Date",
      header: <FieldHeader> Date</FieldHeader>,
      fieldType: "header",
    },
    {
      property: "ResourceGroup",
      header: <FieldHeader> Resource Group</FieldHeader>,
      fieldType: "header",
    },
    {
      property: "ResourceLocation",
      header: <FieldHeader> Resource Location</FieldHeader>,
      fieldType: "header",
    },
    {
      property: "UnitOfMeasure",
      header: <FieldHeader> Unit Of Measure</FieldHeader>,
      fieldType: "header",
    },

    {
      property: "Location",
      header: <FieldHeader> Location</FieldHeader>,
      fieldType: "header",
    },
    {
      property: "ServiceName",
      header: <FieldHeader> Service Name</FieldHeader>,
      fieldType: "header",
    },
  ];

  return (
    <div>
      <ToolBar>
        <FlexBox>
          <TextField>Applications</TextField>
          <Select
            placeholder="Select application"
            options={appList || []}
            value={application}
            onChange={(event: any) => {
              filterList(event.option, resource);
              setApplication(event.option);
            }}
          />
        </FlexBox>
        <FlexBox>
          <TextField>Resources</TextField>
          <Select
            placeholder="Select resource"
            options={resList || []}
            value={resource}
            onChange={(event: any) => {
              filterList(application, event.option);
              setResource(event.option);
            }}
          />
        </FlexBox>
      </ToolBar>
      <Text>
        <Text style={{ fontWeight: "600" }}>{list.length} </Text>
        <Text>{list.length > 1 ? "results" : "result"} of</Text>
        <Text style={{ fontWeight: "600" }}> {rows?.length}</Text>
      </Text>
      {list.length > 0 && (
        <DataTable columns={columns} data={list} paginate={false}></DataTable>
      )}
    </div>
  );
};

export default RowList;
