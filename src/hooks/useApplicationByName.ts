import { useQuery } from "react-query";
import { axiosGet } from "../lib/axiosClient";
import { IRow } from "../common/types";

const useApplicationByName = (name: string) => {
  return useQuery<IRow[]>(
    ["application", name],
    async () => {
      const { data } = await axiosGet(`api/applications/${name}`);
      return data;
    },
    {
      refetchOnWindowFocus: false,
    }
  );
};

export default useApplicationByName;
