import { useQuery } from "react-query";
import { axiosGet } from "../lib/axiosClient";
import { IRow } from "../common/types";

const useRows = () => {
  return useQuery<IRow[]>(
    ["rows"],
    async () => {
      const { data } = await axiosGet("/api/raw");
      return data;
    },
    {
      refetchOnWindowFocus: false,
    }
  );
};

export default useRows;
