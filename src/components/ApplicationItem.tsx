import { FieldHeader } from "../common/style";
import { IItem } from "../common/types";
import { DataTable } from "grommet";
import Spinner from "./Spinner";
import { useApplicationByName } from "../hooks";

const ApplicationItem: React.FC<IItem> = ({ name }) => {
  const { data: applications, isLoading } = useApplicationByName(name);

  const columns = [
    {
      property: "ConsumedQuantity",
      header: <FieldHeader> Consumed Quantity</FieldHeader>,
      fieldType: "header",
    },

    {
      property: "Cost",
      header: <FieldHeader> Cost</FieldHeader>,
      fieldType: "header",
    },
    {
      property: "Date",
      header: <FieldHeader> Date</FieldHeader>,
      fieldType: "header",
    },
    {
      property: "ResourceLocation",
      header: <FieldHeader> Resource Location</FieldHeader>,
      fieldType: "header",
    },
    {
      property: "UnitOfMeasure",
      header: <FieldHeader> Unit Of Measure</FieldHeader>,
      fieldType: "header",
    },

    {
      property: "Location",
      header: <FieldHeader> Location</FieldHeader>,
      fieldType: "header",
    },
    {
      property: "ServiceName",
      header: <FieldHeader> Service Name</FieldHeader>,
      fieldType: "header",
    },
  ];

  if (isLoading) {
    return <Spinner />;
  }

  return (
    <>
      {applications && (
        <DataTable
          columns={columns}
          data={applications}
          paginate={false}
        ></DataTable>
      )}
    </>
  );
};

export default ApplicationItem;
