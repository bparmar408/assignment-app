export interface ITag {
  ["app-name"]: string;
  environment: string;
  ["business-unit"]: string;
}

export interface IRow {
  ConsumedQuantity: number;
  Cost: number;
  InstanceId: string;
  MeterCategory: string;
  ResourceGroup: string;
  ResourceLocation: string;
  Tags: ITag;
  UnitOfMeasure: string;
  Location: string;
  ServiceName: string;
}

export interface IParam {
  appList?: string[];
  resList?: string[];
}

export interface IItem {
  name: string;
}
