import { Accordion, AccordionPanel, Box } from "grommet";
import { IParam } from "../common/types";
import ResourceItem from "../components/ResourceItem";

const ResourceList: React.FC<IParam> = ({ resList }) => {
  return (
    <>
      {resList && (
        <Accordion>
          {resList.map((item, index) => {
            return (
              <AccordionPanel key={index} label={item}>
                <Box pad="medium" background="light-2">
                  <ResourceItem name={item} />
                </Box>
              </AccordionPanel>
            );
          })}
        </Accordion>
      )}
    </>
  );
};

export default ResourceList;
