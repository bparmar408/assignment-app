import styled from "styled-components";
import { Text } from "grommet";

const ActionHeader = styled(Text)`
  font-size: 14px;
  line-height: normal;
  color: #000000;
  font-weight: 600;
`;

const FieldHeader = styled(ActionHeader)`
  min-width: 100px;
`;

export { ActionHeader, FieldHeader };
