import { css } from "styled-components";

const Theme = {
  dataTable: {
    header: {
      color: {
        light: "#000000",
      },
      font: {
        size: "14px",
        weight: "600",
      },
      extend: () => css`
        border-bottom-color: #999999;
        span {
          line-height: 18px;
        }
      `,
    },
  },
  table: {
    body: {
      extend: () => css`
        border-bottom-color: #cccccc;
        border-bottom-style: solid;
        border-bottom-width: 1px;

        span {
          color: #000000;
          font-size: 14px;
          font-weight: 400;
          line-height: 18px;
        }
      `,
    },
  },
  
};
export default Theme;
