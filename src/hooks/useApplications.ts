import { useQuery } from "react-query";
import { axiosGet } from "../lib/axiosClient";

const useApplications = () => {
  return useQuery<string[]>(
    ["applications"],
    async () => {
      const { data } = await axiosGet("api/applications");
      return data;
    },
    {
      refetchOnWindowFocus: false,
    }
  );
};

export default useApplications;
