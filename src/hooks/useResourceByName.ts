import { useQuery } from "react-query";
import { axiosGet } from "../lib/axiosClient";
import { IRow } from "../common/types";

const useResourceByName = (name: string) => {
  return useQuery<IRow[]>(
    ["resource", name],
    async () => {
      const { data } = await axiosGet(`api/resources/${name}`);
      return data;
    },
    {
      refetchOnWindowFocus: false,
    }
  );
};

export default useResourceByName;
