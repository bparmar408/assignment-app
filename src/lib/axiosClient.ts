import axios, { AxiosRequestConfig } from "axios";

const axiosClient = axios.create({
  baseURL: "https://engineering-task.elancoapps.com",
});

const axiosGet = (url: string, params: AxiosRequestConfig = {}) => {
  return axiosClient.get(url, params);
};

export { axiosClient, axiosGet };
    