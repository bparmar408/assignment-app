import { Accordion, AccordionPanel, Box } from "grommet";
import { IParam } from "../common/types";
import ApplicationItem from "../components/ApplicationItem";

const ApplicationList: React.FC<IParam> = ({ appList }) => {
  return (
    <>
      {appList && (
        <Accordion>
          {appList.map((item, index) => {
            return (
              <AccordionPanel key={index} label={item}>
                <Box pad="medium" background="light-2">
                  <ApplicationItem name={item} />
                </Box>
              </AccordionPanel>
            );  
          })}
        </Accordion>
      )}
    </>
  );
};

export default ApplicationList;
